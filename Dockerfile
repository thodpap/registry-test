FROM gcc:11.2

COPY ./Adaptive-AUTOSAR /usr/src/adaptive-autosar

COPY ./DoIP-Lib /usr/src/adaptive-autosar/doip-lib

WORKDIR /usr/src/adaptive-autosar 

RUN gcc --version

RUN apt-get update && apt-get install -y build-essential cmake --no-install-recommends

EXPOSE 80

CMD ["/bin/bash"]